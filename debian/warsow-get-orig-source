#!/bin/sh
set -e

# This script will download the upstream tarball that contains the content of
# the warsow package and create an orig tarball of it.

# Some variables to ease maintanence of this script
WARSOW_VERSION="1.0+dfsg1"
WARSOW_TARBALL="warsow_1.0_sdk.tar.gz"
WARSOW_TARBALL_CHECKSUM="35b9a8f530b51cda15c660b3a73f377e"

USAGE="\
warsow orig tarball download script\n\
This script will generate an orig tarball that's distributed through Debian.\n\
Usage: warsow-get-orig-source [OPTION]\n\
\n\
 -h, --help                 Display this text\n\
--keep-upstream             Don't delete the upstream source tarball\n\
--keep-orig-dir             Don't delete the orig directory\n"

while [ "$#" -gt "0" ]
do
    case "$1" in
        -h|--help)
            echo -e "${USAGE}"
            exit 1
            ;;
        --keep-upstream-tarball)
            KEEP_UPSTREAM_TARBALL=1
            shift
            ;;
        --keep-orig-dir)
            KEEP_ORIG_DIR=1
            shift
            ;;
    esac
done

# List of mirrors to use
MIRROR1=http://prdownloads.sourceforge.net/warsow.mirror
MIRROR2=http://www.warsow.net:1337/~warsow/1.0

if [ ! -f $WARSOW_TARBALL ] ; then
    # Try each mirror until sucessful, else exit with an error
    wget -c $MIRROR1/$WARSOW_TARBALL || \
    wget -c $MIRROR2/$WARSOW_TARBALL || \
    { echo "Could not download upstream source" ; exit 1 ; }
fi

# Verify the checksum
echo -n "Verifying MD5 checksum..."
COMPUTED_CHECKSUM=`md5sum $WARSOW_TARBALL | cut -d ' ' -f 1`
echo "done."

if [ $WARSOW_TARBALL_CHECKSUM != $COMPUTED_CHECKSUM ] ; then
    echo "Checksum verification failed. Checksum was $COMPUTED_CHECKSUM
Expected checksum $WARSOW_TARBALL_CHECKSUM"
    exit 1
else
    echo "Checksum verified. Checksum is $COMPUTED_CHECKSUM."
fi

# Prepare the warsow orig tarball
if [ ! -d warsow-$WARSOW_VERSION ]; then
    echo "Extracting $WARSOW_TARBALL"
    tar -xzf $WARSOW_TARBALL
    mv warsow_1.0_sdk warsow-$WARSOW_VERSION
else
    echo "Already found extracted warsow-$WARSOW_VERSION directory. Please remove or move warsow-$WARSOW_VERSION and also warsow_$WARSOW_VERSION.orig.tar.gz."
    exit 1
fi

# Components removed from upstream tarball.
rm -rf warsow-$WARSOW_VERSION/mapping
rm -rf warsow-$WARSOW_VERSION/modelling
rm -rf warsow-$WARSOW_VERSION/other
rm -rf warsow-$WARSOW_VERSION/libsrcs/libcurl
rm -rf warsow-$WARSOW_VERSION/libsrcs/libfreetype
rm -rf warsow-$WARSOW_VERSION/libsrcs/libjpeg
rm -rf warsow-$WARSOW_VERSION/libsrcs/libogg
rm -rf warsow-$WARSOW_VERSION/libsrcs/libpng
rm -rf warsow-$WARSOW_VERSION/libsrcs/libRocket/libRocket/Samples
rm -rf warsow-$WARSOW_VERSION/libsrcs/libtheora
rm -rf warsow-$WARSOW_VERSION/libsrcs/libvorbis
rm -rf warsow-$WARSOW_VERSION/libsrcs/zlib
rm -rf warsow-$WARSOW_VERSION/source/mac
rm -rf warsow-$WARSOW_VERSION/source/win32

# Create the tarball
if [ ! -f warsow_$WARSOW_VERSION.orig.tar.gz ]; then
    echo "Creating orig tarball."
    tar -czf warsow_$WARSOW_VERSION.orig.tar.gz warsow-$WARSOW_VERSION/
    if [ -z $KEEP_UPSTREAM_TARBALL ]; then
        echo "Removing upstream tarball"
        rm -f $WARSOW_TARBALL
    fi
    if [ -z $KEEP_ORIG_DIR ]; then
        echo "Removing orig directory"
        rm -rf warsow-$WARSOW_VERSION/
    fi
else
    echo "Already found warsow_$WARSOW_VERSION.orig.tar.gz. Please remove or move warsow-$WARSOW_VERSION and also warsow_$WARSOW_VERSION.orig.tar.gz."
    exit 1
fi
